# random-pudding-message
Repositório que armazena as mensagens pudinzescas usadas pelo comando `!pudim` no Nightbot dos canais mais legais da Twitch.

**Para adicionar:**
```javascript
!commands add !pudim $(eval list=$(urlfetch json https://gitlab.com/aln.viana/random-pudding-message/raw/main/messages.json);msg=list[Math.random()*list.length|0];msg=msg.replace(/<USER>/g, "$(user)");msg)
```
